$(document).ready(function() {
  var modal = $('#modal');

  $('#loginButton').on('click', function() {
    $('#modalTitle').html("welcome back");
    $('#forgotPassword').show();
    openModal();
  });

  $('#registerButton').on('click', function() {
    $('#modalTitle').html("register now");
    $('#forgotPassword').hide();
    $('<inputclass="loginInput" placeholder="password" name="password" type="password" maxlength="20">').insertAfter('input[type="password"]');
    $('input[type="submit"]').val("register");
    openModal();
  });

  $('#modal').on('click', function(event) {
    if ($(event.target).attr('class') !== 'modalContent' && !$(event.target).parents('.modalContent').length) {
      closeModal(function() {
        resetList();
      });
    }
  });

  $('#webappLogin').on('click', function(event) {
    event.preventDefault();
    var username = $('input[name=username]').val().toLowerCase();
    var password = $('input[name=password]').val();
    var url = "login.php";
    if ($('input[type="submit"]').val() === "register") {
      url = "register.php";
    }

    $.ajax({
      url: url,
      type: "POST",
      data: {
        username: username,
        password: password
      },
      success: function(data) {
        if (data) {
          $('#errorMessage').html("<strong>" + data.toLowerCase() + "</strong>");
          shakeMessage();
        } else {
          window.location.replace('index.php');
        }
      }
    });
  });

  function shakeMessage() {
    $('#errorMessage').css('animation-name', 'shake');
    setTimeout(function() {
      $('#errorMessage').css('animation-name', 'none');
    }, 900);
  }

  $('#logoutButton').on('click', function() {
    window.location.replace('logout.php');
  });

  $('#listContainer').on('click', function(event) {
    var submitButton = $('input[type="submit"]');
    var eventTarget = $(event.target);
    if (eventTarget.prop('tagName') === "H3") {
      eventTarget = eventTarget.parent();
    }

    if (eventTarget.attr('class') ==='removeList') {
      deleteList(eventTarget);
    } else if (eventTarget.attr('id') === 'addListItem') {
      $('#modalTitle').attr("placeholder", "new list name");
      submitButton.show();
      openModal();
    } else {
      $('#modalTitle').attr("placeholder", "");
      getList(eventTarget);
      getListName(eventTarget);
      submitButton.hide();
      openModal();
    }
  });

  function deleteList(eventTarget) {
    var url = "deleteList.php";
    var list = eventTarget.parent();
    var listID = parseInt(list.attr('id'));

    $.ajax({
      url: url,
      type: "POST",
      data: {
        listID: listID,
      },
      success: function(data) {
        if (data) {
          console.log(data);
          // TODO: handle data
        } else {
          list.remove();
        }
      }
    });
  }

  function getList(eventTarget) {
    var listID = parseInt(eventTarget.attr('id'));
    var url = "getList.php";
    $.ajax({
      url: url,
      type: "POST",
      data: {
        listID: listID,
      },
      success: function(data) {
        $('#taskList').append(data);
      }
    });
  }

  function getListName(eventTarget) {
    var listID = parseInt(eventTarget.attr('id'));
    var url = "getListName.php";
    $.ajax({
      url: url,
      type: "POST",
      data: {
        listID: listID,
      },
      success: function(data) {
        $('#modalTitle').html(data);
      }
    });
  }

  $('#addList').on('click', function(event) {
    event.preventDefault();
    var url = "createList.php";
    var listName = $('#modalTitle').html();
    $.ajax({
      url: url,
      type: "POST",
      data: {
        listName: listName,
      },
      success: function(data) {
        var listID = parseInt(data);
        var htmlString = "<li class='listItem' id='" + listID + "'lID'><h3>"+ listName + "</h3><img class='removeList' src='images/trash.png' alt='remove'></li>";
        $('#addListItem').before(htmlString);
        closeModal(function() {
          resetList();
        });
      }
    });
  });

  $('#taskList').on('click', function(event) {
    var target = $(event.target);
    if (target.attr('id') === "taskList") {
      return;
    }

    if (target.attr('class') === 'deleteTask') {
      var url = "deleteTask.php";
      var taskID = parseInt(target.parent().attr('id'));
      $.ajax({
        url: url,
        type: "POST",
        data: {
          taskID: taskID,
        },
        success: function(data) {
          setTimeout(function() {
            var task = target.parent();
            task.remove();
          }, 1);
        }
      });
    } else if (target.prop('tagName') === 'H3') {
      if (target.attr('contentEditable') === "true") {
        (target.parent()).addClass('taskItemActive');
        if (target.parent().attr('id') === 'addTaskItem') {
          target.html('');
        }
      }
    } else if (target.prop('tagName') === 'IMG') {
      var taskID = parseInt(target.parent().attr('id'));
      var newStatus = 1;
      if (target.attr('src') === "images/untickedCheckbox.png") {
        updateTaskStatus();
        target.attr('src', 'images/tickedCheckbox.png');
        target.next().attr('contentEditable', 'false');
        target.next().css('text-decoration', 'line-through');
        target.next().css('opacity', '0.6');
      } else {
        target.attr('src', 'images/untickedCheckbox.png');
        target.next().attr('contentEditable', 'true');
        target.next().css('text-decoration', 'none');
        target.next().css('opacity', '1');
        newStatus = 0;
      }
      console.log(taskID);
      console.log(newStatus);
      updateTaskStatus(taskID, newStatus);
    } else if (target.attr('id') === 'addTaskItem') {
      target.addClass('taskItemActive');
      target.children('h3').eq(1).html('');
      target.children('h3').eq(1).focus();
    } else if (target.children('h3').attr('contentEditable') === "true") {
      target.addClass('taskItemActive');
      target.children('h3').focus();
    }
  });

  function updateTaskStatus(taskID, newStatus) {
    var url = "updateTaskStatus.php";
    $.ajax({
      url: url,
      type: "POST",
      data: {
        taskID: taskID,
        taskStatus: newStatus
      }
    });
  }

  $('#modalTitle').focusout(function() {
    if ($('#addList').is(':visible')) {
      $('addList').click();
    } else {
      updateListTitle();
    }
  });

  $('#modalTitle').on('keydown paste', function() {
    if($(this).text().length === 40 && event.keyCode != 8) {
      event.preventDefault();
    }
  });

  $('#modalTitle').on('keydown paste', function() {
    if($(this).text().length === 40 && event.keyCode !== 8 && event.keyCode !== 13) {
      event.preventDefault();
    }
  });

  $('#taskList').on('keydown paste', function() {
    if($(this).text().length === 40 && event.keyCode !== 8 && event.keyCode !== 13) {
      event.preventDefault();
    }
  });

  $('#taskList').focusout(function() {
    var eventTarget = $(event.target);
    eventTarget.parent().removeClass('taskItemActive');
    if (eventTarget.parent().attr('id') === "addTaskItem") {
      eventTarget.html("new task");
    } else if (eventTarget.parent().hasClass('taskItem')) {
      updateTaskName(eventTarget);
    }
  });

  $(document).keypress(function(event) {
    if (event.which === 13) {
      event.preventDefault();
      var eventTarget = $(event.target);
      if (eventTarget.parent().attr('id') === 'addTaskItem') {
        addTask(eventTarget);
      } else if (eventTarget.attr('id') === "modalTitle") {
        if ($('#addList').is(':visible')) {
          $('#addList').click();
        } else {
          updateListTitle();
        }
      } else if (eventTarget.parent().hasClass('taskItem')) {
        updateTaskName(eventTarget);
      } else if (eventTarget.attr('name') === "username") {
        eventTarget.blur();
        eventTarget.next().focus();
      } else if (eventTarget.attr('name') === "password") {
        eventTarget.blur();
        $('#webappLogin').click();
      }
    }
  });

  function addTask(eventTarget) {
    if (eventTarget.html() !== "") {
      var taskName = eventTarget.html();
      var url = "createTask.php";
      $.ajax({
        url: url,
        type: "POST",
        data: {
          taskName: taskName,
        },
        success: function(data) {
          var taskID = parseInt(data);
          var htmlString = "<li id='" + taskID + "tID' class='taskItem'><img class='taskStatus' src='images/untickedCheckbox.png'><h3 contenteditable='true'>" + taskName + "</h3><span class='deleteTask'>&times;</span></li>";
          eventTarget.parent().before(htmlString);
          eventTarget.html("");
        }
      });
    }
  }

  function updateListTitle() {
    var listName = $('#modalTitle').html();
    if (listName !== "") {
      var url = "updateList.php";
      $.ajax({
        url: url,
        type: "POST",
        data: {
          listName: listName,
        },
        success: function(data) {
          var listID = parseInt(data); // Returns listID of updated list
          var listItemID = "#" + listID + "lID";
          $(listItemID).children('h3').html(listName);
          $('#modalTitle').blur();
        }
      });
    }
  }

  function updateTaskName(eventTarget) {
    var taskName = eventTarget.html();
    var taskID = eventTarget.parent().attr('id');
    if (taskName !== "") {
      var url = "updateTaskName.php";
      $.ajax({
        url: url,
        type: "POST",
        data: {
          taskName: taskName,
          taskID: taskID
        },
        success: function(data) {
          eventTarget.blur();
        }
      });
    }
  }

  $('.close').on('click', function() {
    closeModal(function() {
      resetList();
    });
  });

  function resetList() {
    if ($('#taskList').length) {
      $('#taskList').empty();
      setTimeout(function() {
        $('#modalTitle').html('');
      }, 500);
    }
  }

  function openModal() {
    modal.css('opacity', '1');
    modal.css('z-index', '1');
  }

  function closeModal(callback) {
    modal.css('opacity', '0');
    setTimeout(function() {
      modal.css('z-index', '-1');
    }, 500);
    typeof callback == "function" && callback();
  }
});
