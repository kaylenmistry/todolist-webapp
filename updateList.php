<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $listID = $_SESSION["activeListID"];
    $listName = $_POST["listName"];

    $updateQuery = $db->prepare("UPDATE lists SET listName=:listName WHERE listID=:listID");
    $updateQuery->bindValue(':listName', $listName, SQLITE3_TEXT);
    $updateQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
    echo $listID;
?>