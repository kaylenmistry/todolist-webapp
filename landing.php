<!doctype html>
<html id="landingHTML">
  <head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <title>todolist</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
  </head>
  <body>
    <div>
      <div class="navBar">
        <ul>
          <li class="navBarItemL"><p><strong>todolist</strong></p></li>
          <li class="navBarItemR">
            <button id="registerButton" class="navBarButton">
              <p>register</p>
            </button>
          </li>
          <li class="navBarItemR">
            <button id="loginButton" class="navBarButton">
              <p>login</p>
            </button>
          </li>
        </ul>
      </div>
      <div id="title">
        <h1>clear the clutter</h1>
      </div>
    </div>
    <div id="modal" class="closed">
      <div class="modalContent">
        <span class="close">&times;</span>
        <h1 id="modalTitle">welcome back</h1>
        <p id="errorMessage"></p>
        <form action="landing.php" method="post">
          <input class="loginInput" placeholder="username" name="username" type="text" maxlength="20">
          <input class="loginInput" placeholder="password" name="password" type="password" maxlength="20">
          <a id="forgotPassword" href="#">forgotten password?</a>
          <input id="webappLogin" name="loginUser" type="submit" value="log in"/>
        </form>
      </div>
    </div>
  </body>
</html>
