<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $taskID = $_POST["taskID"];
    $taskStatus = $_POST["taskStatus"];

    $updateQuery = $db->prepare("UPDATE tasks SET completed=:taskStatus WHERE taskID=:taskID");
    $updateQuery->bindValue(':taskStatus', $taskStatus, SQLITE3_INTEGER);
    $updateQuery->bindValue(':taskID', $taskID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>