<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $taskID = $_POST["taskID"];
    $taskName = $_POST["taskName"];

    $updateQuery = $db->prepare("UPDATE tasks SET taskName=:taskName WHERE taskID=:taskID");
    $updateQuery->bindValue(':taskName', $taskName, SQLITE3_TEXT);
    $updateQuery->bindValue(':taskID', $taskID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>