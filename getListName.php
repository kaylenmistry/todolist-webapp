<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once "database.php";

    $db = new Database;
    $listID = $_POST["listID"];

    $selectQuery = $db->prepare("SELECT listName FROM lists WHERE listID=:listID");
    $selectQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    if ($row = $result->fetchArray()) {
        echo $row['listName'];
    }
?>