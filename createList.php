<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $listName = $_POST["listName"];

    $insertQuery = $db->prepare("INSERT INTO lists VALUES (NULL, :listName)");
    $insertQuery->bindValue(':listName', $listName, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    $rowID = $db->lastInsertRowID();
    $selectQuery = $db->prepare("SELECT listID FROM lists WHERE rowid=:rowID");
    $selectQuery->bindValue(':rowID', $rowID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $listID = $result->fetchArray();
    $listID = $listID['listID'];

    $insertQuery = $db->prepare("INSERT INTO  userLists VALUES (:userID, :listID)");
    $insertQuery->bindValue(':userID', $userID, SQLITE3_TEXT);
    $insertQuery->bindValue(':listID', $listID, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    echo $listID;
?>
