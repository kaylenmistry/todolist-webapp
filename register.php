<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID;
    $username = $db->escapeString($_POST["username"]);
    $password = $db->escapeString($_POST["password"]);
    if (empty($username)) {
        echo "Please enter a username";
        return;
    } elseif (empty($password)) {
        echo "Please enter a password";
        return;
    }

    $existingUserQuery = $db->prepare("SELECT * FROM users WHERE username=:username LIMIT 1");
    $existingUserQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $result = $existingUserQuery->execute();
    if ($result->fetchArray()) {
        echo "Username already exists";
        return;
    }

    $registrationQuery = $db->prepare("INSERT INTO users VALUES (NULL, :username, :password)");
    $registrationQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $registrationQuery->bindValue(':password', $password, SQLITE3_TEXT);
    $result = $registrationQuery->execute();

    $result = $existingUserQuery->execute();
    if ($row = $result->fetchArray()) {
        $userID = $row['username'];
    }
    $_SESSION["username"] = $username;
    $_SESSION["userID"] = $userID;
?>
