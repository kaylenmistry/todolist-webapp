<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once "database.php";

    $db = new Database;
    $listID = $_POST["listID"];
    $_SESSION["activeListID"] = $listID;

    $selectQuery = $db->prepare("SELECT * FROM listTasks INNER JOIN tasks ON listTasks.taskID = tasks.taskID WHERE listTasks.listID=:listID");
    $selectQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $htmlString = "";
    while ($row = $result->fetchArray()) {
        $taskID = $row['taskID'];
        $taskName = $row['taskName'];
        $statusImage = "images/untickedCheckbox.png";
        $style = "";
        $isEditable = "true";
        if ($row['completed']) { 
            $statusImage = "images/tickedCheckbox.png"; 
            $style = " style='text-decoration: line-through; opacity: 0.6;'";
            $isEditable = "false";
        }

        $htmlString .= "<li id='".$taskID."tID' class='taskItem'>\n";
        $htmlString .= "<img class='taskStatus' src='".$statusImage."'><h3 contenteditable='".$isEditable."'".$style.">".$taskName."</h3><span class='deleteTask'>&times;</span>\n";
        $htmlString .= "</li>\n";
    }
    $htmlString .= "<li id='addTaskItem' class='taskItem'>\n";
    $htmlString .= "<h3 id='plusIcon'>+</h3><h3 contenteditable='true'>new task</h3>\n";
    $htmlString .= "</li>";
    echo $htmlString;
?>