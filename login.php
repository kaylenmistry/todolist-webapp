<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $username = $db->escapeString($_POST["username"]);
    $password = $db->escapeString($_POST["password"]);
    if (empty($username)) {
        echo "Please enter a username";
        return;
    } elseif (empty($password)) {
        echo "Please enter a password";
        return;
    }

    $getUserQuery = $db->prepare("SELECT * FROM users WHERE username=:username AND passwordHash=:password LIMIT 1");
    $getUserQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $getUserQuery->bindValue(':password', $password, SQLITE3_TEXT);
    $result = $getUserQuery->execute();
    if ($row = $result->fetchArray()) {
        $_SESSION["userID"] = $row["userID"];
        $_SESSION["username"] = $row["username"];
    } else {
        echo "Incorrect username or password";
    }
?>
