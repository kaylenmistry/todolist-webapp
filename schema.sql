CREATE TABLE IF NOT EXISTS users (
    userID integer PRIMARY KEY,
    username varchar(40) NOT NULL UNIQUE,
    passwordHash varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS lists (
    listID integer PRIMARY KEY,
    listName varchar(40)
);

CREATE TABLE IF NOT EXISTS tasks (
    taskID integer PRIMARY KEY,
    taskName varchar(40),
    completed integer
);

CREATE TABLE IF NOT EXISTS userLists (
    userID integer,
    listID integer,
    PRIMARY KEY (userID, listID),
    FOREIGN KEY (userID) REFERENCES users (userID)
    ON DELETE CASCADE ON UPDATE NO ACTION,
    FOREIGN KEY (listID) REFERENCES lists (listID)
    ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS listTasks (
    listID integer,
    taskID integer,
    PRIMARY KEY (listID, taskID),
    FOREIGN KEY (listID) REFERENCES lists (listID)
    ON DELETE CASCADE ON UPDATE NO ACTION,
    FOREIGN KEY (taskID) REFERENCES tasks (taskID)
    ON DELETE CASCADE ON UPDATE NO ACTION
);
