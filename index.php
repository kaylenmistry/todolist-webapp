<?php
  header('Content-type: text/html; charset=utf-8');
  session_start();
  if (!isset($_SESSION["userID"]) || !isset($_SESSION["username"])) {
    header("Location: landing.php");
    die();
  }
?>
<!doctype html>
<html id="standardHTML">
  <head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <title>todolist</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
  </head>
  <body>
      <div class="navBar">
        <ul>
          <li class="navBarItemL"><p><strong>todolist</strong></p></li>
          <li class="navBarItemR">
            <button id="logoutButton" class="navBarButton">
              <p>logout</p>
            </button>
          </li>
        </ul>
      </div>
      <ul id="listContainer">
        <?php
          error_reporting(E_ALL);
          ini_set('display_errors','on');
          include "database.php";

          $db = new Database;
          $userID = $_SESSION["userID"];

          $selectQuery = $db->prepare('SELECT * FROM userLists INNER JOIN lists ON userLists.listID = lists.listID WHERE userID=:userID');
          $selectQuery->bindValue(':userID', $userID, SQLITE3_TEXT);
          $result = $selectQuery->execute();

          while ($row = $result->fetchArray()) {
            $listHTML =
              "<li class=\"listItem\" id=\"".$row['listID']."lID\">
                <h3>".$row['listName']."</h3>
                <img class=\"removeList\" src=\"images/trash.png\" alt=\"remove\">
              </li>";
            echo $listHTML;
          }
          $db->__destruct();
        ?>
        <li id="addListItem" class="listItem">
          <h3>+ add list</h3>
        </li>
      </ul>
      <div id="modal" class="closed">
        <div class="modalContent">
          <span class="close">&times;</span>
          <h1 id="modalTitle" contenteditable="true" placeholder=""></h1>
          <ul id="taskList"></ul>
          <form>
            <input id="addList" type="submit" value="add list"/>
          </form>
        </div>
    </div>
  </body>
</html>
