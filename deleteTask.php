<?php

    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $listID = $_SESSION["activeListID"];
    $taskID = $_POST["taskID"];

    $deleteQuery = $db->prepare("DELETE FROM tasks WHERE taskID=:taskID");
    $deleteQuery->bindValue(':taskID', $taskID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();

    $deleteQuery = $db->prepare("DELETE FROM listTasks WHERE listID=:listID AND taskID=:taskID");
    $deleteQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $deleteQuery->bindValue(':taskID', $taskID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();
?>