<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $listID = $_POST["listID"];

    $deleteQuery = $db->prepare("DELETE FROM lists WHERE listID=:listID");
    $deleteQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();

    $deleteQuery = $db->prepare("DELETE FROM userLists WHERE userID=:userID AND listID=:listID");
    $deleteQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $deleteQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();

    $deleteQuery = $db->prepare("DELETE FROM listTasks WHERE listID=:listID");
    $deleteQuery->bindValue(':listID', $listID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();
?>