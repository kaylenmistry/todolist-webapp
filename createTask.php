<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $listID = $_SESSION["activeListID"];
    $taskName = $db->escapeString($_POST["taskName"]);

    $insertQuery = $db->prepare("INSERT INTO tasks VALUES (NULL, :taskName, 0)");
    $insertQuery->bindValue(':taskName', $taskName, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    $rowID = $db->lastInsertRowID();
    $selectQuery = $db->prepare("SELECT taskID FROM tasks WHERE rowid=:rowID");
    $selectQuery->bindValue(':rowID', $rowID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $taskID = $result->fetchArray();
    $taskID = $taskID['taskID'];

    $insertQuery = $db->prepare("INSERT INTO  listTasks VALUES (:listID, :taskID)");
    $insertQuery->bindValue(':listID', $listID, SQLITE3_TEXT);
    $insertQuery->bindValue(':taskID', $taskID, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    echo $taskID;
?>
